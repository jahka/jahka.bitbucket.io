var localStorageAPI = {
	// This method may not be needed as we go along
	// the support is becoming better and better day-by-day
	// http://caniuse.com/#feat=namevalue-storage
	// better to be safe than sorry or get script errors :|
	isSupported: function() {
		return window.localStorage;
	},
	setItem: function(key, value) {
		return localStorage.setItem(key, value);
	},
	getItem: function(key) {
		return localStorage.getItem(key);
	},
	// If do not want to build a wrapper like how I did here but implement 
	// setObject() and getObject(), you can create prototype methods on  
	// Storage
	// Storing Objects in HTML5 localStorage : http://stackoverflow.com/a/3146971/1015046 
	setObject: function(key, object) {
		return localStorage.setItem(key, JSON.stringify(object));
	},
	getObject: function(key) {
		try {
			return JSON.parse(localStorage.getItem(key));
		}
		catch(e) {
			return {};
		}
	},
	removeItem: function(key) {
		return localStorage.removeItem(key);
	},
	clearAll: function() {
		return localStorage.clear();
	}
};