

var Data = {
	data: {},

	load: function() {
		this.data = localStorageAPI.getObject("default");

		if(this.data && this.data.meals)
			Meal.import(this.data.meals);
	},

	save: function() {
		this.data.meals = Meal.export();

		localStorageAPI.setObject("default", this.data);
	}
};

$(document).ready(function() {
	if(!localStorageAPI.isSupported()) {
		alert("Local storage is not supported");
	}

	Data.load();

	console.log(Data.data);

	refreshDailyTracks();

	$("#debug").html("loaded");

	$("#set_target_button").on('click', function(event) {
		$("#main_menu_div").fadeOut();
		$("#set_target_div").fadeIn();
		$("#unit_selector").fadeIn();
		$("#debug").html("click1");
		event.preventDefault();
		return false;
	});


	$("#list_meals_button").on('click', function() {
		$("#main_menu_div").fadeOut();
		$("#meal_list_div").fadeIn();
		Meal.renderList("#meal_list", "Meal.load", true);
	});

	$("#track_macros_button").on('click', function() {
		$("#main_menu_div").fadeOut();
		$("#track_div").fadeIn();
		$("#unit_selector").fadeIn();


		$("#track_name").val("");
		$("#track_fats").val(0);
		$("#track_carbs").val(0);
		$("#track_protein").val(0);
		$("track_serving_size").val("");
		$("tracked_serving").val("");
		$("#tracked_weight").val("");
	});

	$("#men_bfp").on('click', function() {
		$(this).fadeOut();
	});

	$("#women_bfp").on('click', function() {
		$(this).fadeOut();
	});

	$("#activity_level_info_close").on('click', function() {
		$(this).fadeOut();
	});

	$("#height_ft").on("input", function() {
		var inches = parseInt($("#height_ft").val())*12 + parseInt($("#height_in").val());
		$("#height_cm").val(Math.round(inches*2.54));
	});

	$("#height_in").on("input", function() {
		var inches = parseInt($("#height_ft").val())*12 + parseInt($("#height_in").val());
		$("#height_cm").val(Math.round(inches*2.54));
	});

	$("#weight_lb").on("input", function() {
		$("#weight_kg").val(Math.round(parseInt($("#weight_lb").val() * 0.453592)));
	});

});

function backToMenu() {
	refreshDailyTracks();

	$("#main_menu_div").fadeIn();
	$("#set_target_div").fadeOut();
	$("#meal_list_div").fadeOut();
	$("#unit_selector").fadeOut();
	$("#track_div").fadeOut();
}

function guess_bfp() {
	if($('input[name=gender]:checked').val() == "male") {
		$("#men_bfp").fadeIn();
		$("#set_target_div").fadeOut();
	}
	else {
		$("#women_bfp").fadeIn();
		$("#set_target_div").fadeOut();
	}
}

function activity_level_info() {
	if($("#activity_level_info").css("display") == "none") {
		$("#activity_level_info").fadeIn();
		$('#set_target_div').fadeOut();
	}
	else {
		$("#activity_level_info").fadeOut();
		$('#set_target_div').fadeIn();
	}
}

function setMetric() {
	$("#set_metric").addClass("selected");
	$("#set_imperial").removeClass("selected");

	$("#metric_height").show();
	$("#imperial_height").hide();

	$("#metric_weight").show();
	$("#imperial_weight").hide();
}

function setImperial() {
	$("#set_imperial").addClass("selected");
	$("#set_metric").removeClass("selected");

	$("#metric_height").hide();
	$("#imperial_height").show();

	$("#metric_weight").hide();
	$("#imperial_weight").show();

	var inches = parseInt($("#height_ft").val())*12 + parseInt($("#height_in").val());
	$("#height_cm").val(Math.round(inches*2.54));
	$("#weight_kg").val(Math.round(parseInt($("#weight_lb").val() * 0.453592)));
}



function calculateTarget() {
	var lbm = parseInt($("#weight_kg").val()) * (100 - $("#body_fat").val())/100;

	var bmr = 370 + (21.6 * lbm);

	var amr = parseFloat($("#activity_level").val()) * bmr;

	var add_cal = 0;
	var carbs = 0.4;
	var prots = 0.3;
	var fats = 0.3;

	switch($("#weight_goal").val()) {
		case "loose":
			add_cal = -300;
			carbs = 0.2;
			fats = 0.35;
			prots = 0.45;
		break;
		case "gain":
			add_cal = 300;
			carbs = 0.5;
			fats = 0.2;
			prots = 0.3;
		break;
	}

	var target = Math.round(amr + add_cal);

	$("#target_total_calories").html(
		"TARGET: " + target + " Cal<br>"
		);

	$("#target_carbs").html(
		carbs*100 + " %<br>" +
		Math.round(target*carbs/4) + " g"
		);

	$("#target_fats").html(
		fats*100 + " %<br>" +
		Math.round(target*fats/9) + " g"
		);

	$("#target_protein").html(
		prots*100 + " %<br>" +
		Math.round(target*prots/4) + " g"
		);
}

function showError(id) {
	$(id).addClass("animated shake");
	$(id).one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
		$(id).removeClass("animated shake");
	});
}


function selectMealToTrack() {
	$("#unit_selector").fadeOut();
	$("#track_div").fadeOut();
	$("#meal_select_list_div").fadeIn();
	Meal.renderList("#meal_select_list", "selectMeal");
}

function backToTracking() {
	$("#unit_selector").fadeIn();
	$("#track_div").fadeIn();
	$("#meal_select_list_div").fadeOut();
}

function selectMeal(meal_index) {
	var meal = Meal.list[meal_index];

	$("#track_serving_size").val(meal.serving_size == "" ? 100 : meal.serving_size);

	$("#track_name").val(meal.name);
	$("#track_fats").val(meal.fatsPer100g());
	$("#track_carbs").val(meal.carbsPer100g());
	$("#track_protein").val(meal.proteinPer100g());

	backToTracking();
}

function getNonEmptyString(id) {
	var val = $(id).val();

	if(val == "") {
		showError(id);
		throw "Error";
		return;
	}

	return val;	
}

function getNumberValue(id) {
	var val = parseFloat($(id).val());

	if(val < 0 || isNaN(val)) {
		showError(id);
		throw "Error";
		return;
	}

	return val;
}

function getPositiveValue(id) {
	var val = parseFloat($(id).val());

	if(val <= 0 || isNaN(val)) {
		showError(id);
		throw "Error";
		return;
	}

	return val;
}

function trackByWeight() {

	try {
		var name = getNonEmptyString("#track_name");
		var fats = getNumberValue("#track_fats");
		var carbs = getNumberValue("#track_carbs");
		var protein = getNumberValue("#track_protein");
		var grams = getPositiveValue("#tracked_weight");

		if(Data.data.tracked == undefined)
			Data.data.tracked = [];

		Data.data.tracked.push({
			time: Date.now(),
			name: name,
			fats: fats,
			carbs: carbs,
			protein: protein,
			grams: grams,
			servings: 0,
			serving_size: 0
		});

		Data.save();

		$('#track_buttons').show();
		$('#add_by_weight').hide();

		backToMenu();
	}
	catch(e) {}
}

function trackByServing() {
	try {
		var name = getNonEmptyString("#track_name");
		var fats = getNumberValue("#track_fats");
		var carbs = getNumberValue("#track_carbs");
		var protein = getNumberValue("#track_protein");

		var serving_size = getPositiveValue("#track_serving_size");
		var servings = getPositiveValue("#tracked_serving");

		if(Data.data.tracked == undefined)
			Data.data.tracked = [];

		Data.data.tracked.push({
			time: Date.now(),
			name: name,
			fats: fats,
			carbs: carbs,
			protein: protein,
			grams: 0,
			servings: servings,
			serving_size: serving_size
		});

		Data.save();

		$('#track_buttons').show();
		$('#add_by_serving').hide();

		backToMenu();
	}
	catch(e) {}
}

function refreshDailyTracks() {
	var html = "";

	var date = new Date();
	var now = Date.now();
	var milliseconds_in_a_day = 24*60*60*1000;
	var milliseconds_in_a_minute = 60*1000;
	var offset = date.getTimezoneOffset();

	if(Data.data.tracked == undefined) {
		html = "<li>No tracked entries today</li>";
	}
	else {
		Data.data.tracked.forEach(function(track) {
			if((now - (track.time - offset*milliseconds_in_a_minute)) / milliseconds_in_a_day < 1)
				html += '<li><a href="#" class="macroentry">';

				var daily_minutes = Math.floor((track.time % milliseconds_in_a_day) / milliseconds_in_a_minute) - offset;

				var hour = Math.floor(daily_minutes / 60);
				var minutes = daily_minutes % 60;

				if(hour < 10)
					hour = "0" + hour;
				if(minutes < 10)
					minutes = "0" + minutes;

				html += hour + ":" + minutes + " ";
				html += track.name;

				html += '</a></li>';
		});
	}

	$("#todays_entries").html(html);
}