var Meal = {
	list: [],
	current: undefined,
	editing: false,

	Meal: function(meal_data = {name: "", serving_size: "", ingredients: []}) {
		console.log("New meal", meal_data);
		this.name = meal_data.name;
		this.serving_size = meal_data.serving_size == undefined ? "" : meal_data.serving_size;
		this.ingredients = meal_data.ingredients;

		this.original_name = meal_data.name;
		this.original_serving_size = meal_data.serving_size;
		this.original_ingredients_str = JSON.stringify(meal_data.ingredients);

		this.edited_ingredient_index = -1;
		this.edit_item = false;

		console.log(this);

		this.totalWeight = function() {
		    var total_amount = 0;

		    var meal = this;

		    this.ingredients.forEach(function(ingredient, index) {
		    	total_amount += ingredient.amount;
		    });

		    return total_amount;
		};

		this.fatsPer100g = function() {
			var total_fats = 0;
		    var total_amount = 0;

		    var meal = this;

		    this.ingredients.forEach(function(ingredient, index) {
		    	total_fats += ingredient.fats/100*ingredient.amount;
		    	total_amount += ingredient.amount;
		    });

		    if(total_amount > 0)
		    	return Math.round(1000*total_fats/total_amount)/10;
		    else
		    	return 0;
		}

		this.carbsPer100g = function() {
			var total_carbs = 0;
		    var total_amount = 0;

		    var meal = this;

		    this.ingredients.forEach(function(ingredient, index) {
		    	total_carbs += ingredient.carbs/100*ingredient.amount;
		    	total_amount += ingredient.amount;
		    });

		    if(total_amount > 0)
		    	return Math.round(1000*total_carbs/total_amount)/10;
		    else
		    	return 0;
		}

		this.proteinPer100g = function() {
			var total_protein = 0;
		    var total_amount = 0;

		    var meal = this;

		    this.ingredients.forEach(function(ingredient, index) {
		    	total_protein += ingredient.protein/100*ingredient.amount;
		    	total_amount += ingredient.amount;
		    });

		    if(total_amount > 0)
		    	return Math.round(1000*total_protein/total_amount)/10;
		    else
		    	return 0;
		}



		this.render = function(dom_element_id) {
			var html = '<table class="u-full-width" style="table-layout:fixed;">'
					+ '<thead>'
		              + '<tr>'
		                + '<th width="30%">Ingredient</th>'
		                + '<th>Fat</th>'
		                + '<th>Carbs</th>'
		                + '<th>Protein</th>'
		                + '<th>Amount</th>'
		              + '</tr>'
		            + '</thead>'
		            + '<tbody>';

		    if(this.ingredients.length == 0) {
		    	html += '<tr><td colspan="5">Add ingredients below to create a meal.</td></tr>'
		    }

		    var total_carbs = 0;
		    var total_fats = 0;
		    var total_protein = 0;
		    var total_amount = 0;

		    var meal = this;

		    this.ingredients.forEach(function(ingredient, index) {
		    	total_carbs += ingredient.carbs/100*ingredient.amount;
		    	total_fats += ingredient.fats/100*ingredient.amount;
		    	total_protein += ingredient.protein/100*ingredient.amount;
		    	total_amount += ingredient.amount;

		    	html += '<tr ';
		    	if(meal == Meal.current)
		    	 	html += 'onclick="Meal.editIngredient(this, ' + index + ')"';
		    	html += '>';
		    	html += '<td>' + ingredient.name + '</td>';
		    	html += '<td>' + ingredient.fats + '<span class="small">/100g</span></td>';
		    	html += '<td>' + ingredient.carbs + '<span class="small">/100g</span></td>';
		    	html += '<td>' + ingredient.protein + '<span class="small">/100g</span></td>';
		    	html += '<td>' + ingredient.amount + '<span class="small">g</span></td>';
		    	html += '</tr>'
		    });

		    if(total_amount > 0) {

			    html += '<tr style="border-top: 2px solid black; font-weight: bold;">';
			    html += '<td>Total</td>';
				html += '<td>' + Math.round(total_fats) + '<span class="small">g</span></td>';
				html += '<td>' + Math.round(total_carbs) + '<span class="small">g</span></td>';
				html += '<td>' + Math.round(total_protein) + '<span class="small">g</span></td>';
				html += '<td>' + Math.round(total_amount) + '<span class="small">g</span></td>';
			    html += '</tr>';

			    html += '<tr style="font-weight: bold;">';
			    html += '<td>/100g</td>';
				html += '<td>' + Math.round(100*total_fats/total_amount) + '<span class="small">g</span></td>';
				html += '<td>' + Math.round(100*total_carbs/total_amount) + '<span class="small">g</span></td>';
				html += '<td>' + Math.round(100*total_protein/total_amount) + '<span class="small">g</span></td>';
				html += '<td></td>';
			    html += '</tr>';

			    var total_calories = total_carbs*4 + total_protein*4 + total_fats*9;

			    html += '<tr style="font-weight: bold;">';
			    html += '<td>% Cal</td>';
				html += '<td>' + Math.round(100*total_fats*9/total_calories) + '<span class="small">%</span></td>';
				html += '<td>' + Math.round(100*total_carbs*4/total_calories) + '<span class="small">%</span></td>';
				html += '<td>' + Math.round(100*total_protein*4/total_calories) + '<span class="small">%</span></td>';
				html += '<td></td>';
			    html += '</tr>';
			}

		    html += '</tbody></table>';

		    $(dom_element_id).html(html);
		}

		this.editIngredient = function(name, carbs, fats, protein, amount) {
			this.ingredients[this.edited_ingredient_index].name = name;
			this.ingredients[this.edited_ingredient_index].carbs = carbs;
			this.ingredients[this.edited_ingredient_index].fats = fats;
			this.ingredients[this.edited_ingredient_index].protein = protein;
			this.ingredients[this.edited_ingredient_index].amount = amount;
		}

		this.addIngredient = function(name, carbs, fats, protein, amount) {
			this.ingredients.push({
				name: name,
				carbs: carbs,
				fats: fats,
				protein: protein,
				amount: amount,
			});
		}

		this.reset = function() {
			this.name = this.original_name;
			this.serving_size = this.original_serving_size;
			this.ingredients = JSON.parse(this.original_ingredients_str);
		}
	},

	addIngredient: function() {
		$("#add_ingredient_heading").hide();
		$("#add_ingredient_buttons").hide();
		$("#save_meal_buttons").hide();
		$("#delete_meal_button").hide();
		$("#ingredient_entry").show();
		$("#ingredient_name").focus();
		$("#remove_edit_ingredient_button").hide();
		$("#ok_ingredient_button").show();
		$("#ok_edit_ingredient_button").hide();
	},

	okIngredient: function() {
		var name = $("#ingredient_name").val();
		var carbs = parseFloat($("#ingredient_carbs").val());
		var fats = parseFloat($("#ingredient_fats").val());
		var protein = parseFloat($("#ingredient_protein").val());
		var amount = parseFloat($("#ingredient_amount").val());

		if(name == "") {
			showError("#ingredient_name");
			return;
		}

		if(isNaN(carbs)) {
			showError("#ingredient_carbs");
			return;
		}

		if(isNaN(fats)) {
			showError("#ingredient_fats");
			return;
		}

		if(isNaN(protein)) {
			showError("#ingredient_protein");
			return;
		}

		if(isNaN(amount)) {
			showError("#ingredient_amount");
			return;
		}

		if(this.current == undefined)
			this.current = new this.Meal();

		this.current.addIngredient(name, carbs, fats, protein, amount);

		this.cancelEditIngredient();

		this.current.render("#meal_table");
	},

	editIngredient: function(element, index) {
		$("#create_meal_div tr").removeClass("selected");
		$(element).addClass("selected");

		$("#add_ingredient_heading").hide();
		$("#add_ingredient_buttons").hide();
		$("#save_meal_buttons").hide();
		$("#delete_meal_button").hide();
		$("#ingredient_entry").show();
		$("#remove_edit_ingredient_button").show();
		$("#ok_ingredient_button").hide();
		$("#ok_edit_ingredient_button").show();

		//$("#edit_ingredient_button").show();
		//$("#cancel_edit_ingredient_button").show();
		//$("#remove_edit_ingredient_button").show();
		// $("#save_meal_buttons").hide();

		// if(this.editing)
		// 	$("#delete_meal_button").hide();

		var ing = this.current.ingredients[index];

		$("#ingredient_name").val(ing.name);
		$("#ingredient_carbs").val(ing.carbs);
		$("#ingredient_fats").val(ing.fats);
		$("#ingredient_protein").val(ing.protein);
		$("#ingredient_amount").val(ing.amount);

		this.current.edited_ingredient_index = index;
	},

	cancelEditIngredient: function() {
		$("#create_meal_div tr").removeClass("selected");

		$("#add_ingredient_heading").show();
		$("#add_ingredient_buttons").show();
		$("#save_meal_buttons").show();
		if(this.editing)
			$("#delete_meal_button").show();
		$("#ingredient_entry").hide();

		$("#ingredient_name").val("");
		$("#ingredient_carbs").val("");
		$("#ingredient_fats").val("");
		$("#ingredient_protein").val("");
		$("#ingredient_amount").val("");
	},

	applyEditIngredient: function() {
		var name = $("#ingredient_name").val();
		var carbs = parseFloat($("#ingredient_carbs").val());
		var fats = parseFloat($("#ingredient_fats").val());
		var protein = parseFloat($("#ingredient_protein").val());
		var amount = parseFloat($("#ingredient_amount").val());

		if(name == "") {
			showError("#ingredient_name");
			return;
		}

		if(isNaN(carbs)) {
			showError("#ingredient_carbs");
			return;
		}

		if(isNaN(fats)) {
			showError("#ingredient_fats");
			return;
		}

		if(isNaN(protein)) {
			showError("#ingredient_protein");
			return;
		}

		if(isNaN(amount)) {
			showError("#ingredient_amount");
			return;
		}


		this.current.editIngredient(name, carbs, fats, protein, amount);
		this.cancelEditIngredient();
		this.current.render("#meal_table");
	},

	removeEditedIngredient: function() {
		this.current.ingredients.splice(this.current.edited_ingredient_index, 1);
		this.cancelEditIngredient();
		this.current.render("#meal_table");
	},

	save: function() {
		if($("#meal_name").val() == "") {
			showError("#meal_name");
			return;
		}

		if(this.current == undefined || this.current.ingredients.length == 0) {
			showError("#add_ingredient_button");
			return;
		}

		this.current.name = $("#meal_name").val();

		this.current.serving_size = $("#serving_size_value").val();
		if(this.current.serving_size == "")
			this.current.serving_size = this.current.totalWeight();

		var self = this;

		var found = false;
		this.list.forEach(function(meal) {
			if(meal == self.current)
				found = true;
		});

		if(!found) {
			console.log("saving creating a new meal:", this.current.name);
			this.list.push(this.current);
		}

		Data.save();

		this.current.original_ingredients_str = JSON.stringify(this.current.ingredients);
		this.current.original_name = this.current.name;
		this.current.original_serving_size = this.current.serving_size;

		this.current = undefined;

		this.cancelCreate();
	},

	load: function(meal_index) {
		this.current = this.list[meal_index];

		this.editing = true;

		console.log("load", meal_index, this.current);

		$("#meal_list_div").fadeOut();
		$("#create_meal_div").fadeIn();
		$("#unit_selector").fadeIn();
		$("#delete_meal_button").show();
		$("#save_meal_button").show();
		$("#reset_meal_button").show();
		$("#save_meal_buttons").show();

		$("#meal_title").text(this.current.name);
		$("#meal_name").val(this.current.name);
		$("#serving_size_value").val(this.current.serving_size ? this.current.serving_size : "");
		this.current.render("#meal_table");
	},

	reset: function() {
		if(this.current) {
			this.current.reset();
			this.current.render("#meal_table");
			$("#meal_name").val(this.current.name);
		}
		else
			$("#meal_name").val("");

		this.cancelEditIngredient();
	},

	import: function(list) {
		var Meal = this;
		this.list = [];

		list.forEach(function(meal) {
			Meal.list.push(new Meal.Meal(JSON.parse(JSON.stringify(meal))));
		});
	},

	export: function() {
		var list = [];

		this.list.forEach(function(meal) {
			list.push({
				name: meal.name,
				serving_size: meal.serving_size,
				ingredients: meal.ingredients
			});
		});

		return list;
	},

	delete: function() {
		var self = this;
		var found_index = -1;
		this.list.forEach(function(meal, meal_index) {
			if(meal == self.current)
				found_index = meal_index;
		});

		if(found_index >= 0)
			this.list.splice(found_index, 1);

		Data.save();

		this.cancelCreate();
	},

	create: function() {
		$("#meal_list_div").fadeOut();
		$("#create_meal_div").fadeIn();
		$("#unit_selector").fadeIn();
		$("#delete_meal_button").hide();
		$("#save_meal_button").show();
		$("#reset_meal_button").hide();

		this.editing = false;

		$("#meal_title").text("Create a new meal");
		$("#meal_name").val("").focus();
		$("#serving_size_value").val("");
		this.current = new this.Meal();
		this.current.render("#meal_table");
		this.cancelEditIngredient();
	},

	cancelCreate: function() {
		$("#meal_list_div").fadeIn();
		$("#create_meal_div").fadeOut();
		$("#unit_selector").fadeOut();
		this.renderList("#meal_list", "Meal.load", true);
	},

	renderList: function(dom_element_id, function_name, create_new = false) {
		var html = "<ul>";

		if(create_new)
			html += '<li class="list_meal"><a href="javascript:Meal.create()">Create a new meal</a></li>'

		this.list.forEach(function(meal, meal_index) {
			html += '<li class="list_meal"><a href="javascript:' + function_name + '(' + meal_index + ')">' + meal.name + '</a></li>';
		});

		html += "</ul>";

		$(dom_element_id).html(html);
	},

	setServing: function(factor) {
		$("#serving_size_value").val(Math.round(this.current.totalWeight()*factor));
	}
}